package net.vmuzychko;

import java.util.Objects;


public class Item
{
    private String description;

    public Item()
    {
    }

    public Item(String description)
    {
        this.description = description;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Item item = (Item) o;
        return Objects.equals(description, item.description);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(description);
    }
}
