package net.vmuzychko;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.*;


public class LfuCacheTest
{
    private static final int CAPACITY = 10;
    private LfuCache<String, Item> cache;

    @Before
    public void setUp() throws Exception
    {
        cache = new LfuCache<>(CAPACITY);
    }

    @Test
    public void shouldInsertDataWithZeroFrequency() {
        // given
        insertDataToCache(CAPACITY);

        // then
        Map<String, Integer> keys = cache.getKeys();
        keys.forEach((key, value) -> assertEquals(0, value.intValue()));
    }

    @Test
    public void shouldInsertDataToCache() {
        // when
        cache.put("key 1", new Item("value 1"));
        cache.put("key 2", new Item("value 2"));

        // then
        assertTrue(cache.getKeys().containsKey("key 1"));
        assertTrue(cache.getKeys().containsKey("key 2"));
        assertTrue(cache.get("key 1").isPresent());
        assertTrue(cache.get("key 2").isPresent());
    }

    @Test
    public void shouldIncreaseKeyFrequencyAfterGetRequest() {
        // given
        insertDataToCache(CAPACITY);

        // when
        cache.get("key 3");
        cache.get("key 3");
        cache.get("key 2");

        // then
        assertEquals(2, cache.getKeys().get("key 3").intValue());
        assertEquals(1, cache.getKeys().get("key 2").intValue());
    }

    @Test
    public void shouldInvalidateLeastFrequentlyUsedKey() {
        // given
        int testCapacity = 3;
        cache = new LfuCache<>(testCapacity);
        insertDataToCache(testCapacity);

        // when
        cache.get("key 0");
        cache.get("key 0");
        cache.get("key 2");
        cache.put("new key", new Item("new value"));

        // then
        assertEquals(Optional.empty(), cache.get("key 1"));
    }

    @Test
    public void shouldNotExceedCapacity() {
        // given
        insertDataToCache(CAPACITY);

        // when
        cache.put("new key", new Item("new value"));

        // then
        assertEquals(CAPACITY, cache.getKeys().size());
        assertEquals(CAPACITY, cache.size());
    }

    @Test
    public void shouldEvictExistingKey() {
        // given
        insertDataToCache(CAPACITY);

        // when
        cache.evict("key 3");

        // then
        assertEquals(Optional.empty(), cache.get("key 3"));
    }

    @Test (expected = NoSuchElementException.class)
    public void shouldEvictNotExistingKey() {
        // given
        insertDataToCache(CAPACITY);

        // when
        cache.evict("wrong key");
    }

    @Test
    public void shouldClearCache() {
        // given
        insertDataToCache(CAPACITY);

        // when
        cache.clear();

        // then
        assertEquals(0, cache.getKeys().size());
        assertEquals(0, cache.size());
    }

    private void insertDataToCache(int size)
    {
        for (int i = 0; i < size; i++) {
            cache.put("key " + i, new Item("value " + i));
        }
    }
}