package net.vmuzychko;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;


public class LruCacheTest
{
    private LruCache<String, Item> lruCache = new LruCache<>(TEST_CAPACITY);

    public static final int TEST_CAPACITY = 100;

    @Before
    public void setUp() {
        lruCache = new LruCache<>(TEST_CAPACITY);
    }

    @Test
    public void shouldPutObjectToCache() {
        // given
        lruCache.put("key", new Item("value"));

        // when
        Optional<Item> key = lruCache.get("key");

        // then
        assertTrue(key.isPresent());
    }

    @Test
    public void shouldStoreCorrectValueForKeyAfterRequest() {
        // given
        insertDataToCache(TEST_CAPACITY);

        // when
        Optional<Item> item = lruCache.get("key 3");

        // then
        assertEquals("value 3", item.get().getDescription());
    }

    @Test
    public void shouldLiftObjectIndexAfterRequest() {
        // given
        insertDataToCache(TEST_CAPACITY);

        // when
        lruCache.get("key 3");

        // then
        assertEquals(0, lruCache.getKeys().indexOf("key 3"));
        assertEquals(TEST_CAPACITY - 1, lruCache.getKeys().indexOf("key 0"));
    }

    @Test
    public void shouldNotIncreaseCacheSizeMoreThenCapacity() {
        // given
        insertDataToCache(TEST_CAPACITY);

        // when
        Item specialItem = new Item("value special");
        lruCache.put("key special", specialItem);

        // then
        assertEquals(TEST_CAPACITY, lruCache.getKeys().size());
    }

    @Test
    public void shouldInvalidateLeastRequestedItem() {
        // given
        insertDataToCache(TEST_CAPACITY);
        String leastRequestedKey = "key 0";

        // when
        Item specialItem = new Item("value special");
        lruCache.put("key special", specialItem);

        // then
        assertEquals("key 1", lruCache.getKeys().get(TEST_CAPACITY - 1));
        assertEquals(-1, lruCache.getKeys().indexOf(leastRequestedKey));
        assertEquals(Optional.empty(), lruCache.get(leastRequestedKey));
        assertEquals("key special", lruCache.getKeys().get(0));
        assertEquals(TEST_CAPACITY, lruCache.getKeys().size());
    }


    @Test
    public void shouldClearCacheData() {
        // given
        insertDataToCache(TEST_CAPACITY);

        // when
        lruCache.clear();

        // then
        assertEquals(0, lruCache.getKeys().size());
    }

    @Test
    public void shouldEvictData() {
        // given
        insertDataToCache(TEST_CAPACITY);

        // when
        lruCache.evict("key 2");

        // then
        assertEquals(TEST_CAPACITY - 1, lruCache.getKeys().size());
        assertEquals(Optional.empty(), lruCache.get("key 2"));
    }

    @Test (expected = NoSuchElementException.class)
    public void shouldThrowExceptionForIncorrectKeyWhenEvict() {
        // given
        insertDataToCache(TEST_CAPACITY);

        // when
        lruCache.evict("key wrong");
    }


    @Test
    public void shouldReturnEmptyDataForWrongKey() {
        // given
        insertDataToCache(TEST_CAPACITY);

        // when
        Optional<Item> wrong = lruCache.get("wrong");

        // then
        assertEquals(Optional.empty(), wrong);
    }

    @Test
    public void shouldNotPutExistingKeyToTheZeroIndex() {

        // given
        insertDataToCache(10);

        // when
        lruCache.put("key 5", new Item("new value"));

        // then
        Map<String, Long> keysFrequency = lruCache.getKeys().stream().collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        assertEquals(1L, keysFrequency.get("key 5").longValue());
    }

    @Test
    public void shouldNotPutExistingKeyToTheZeroIndexCapacityExceeded() {

        // given
        insertDataToCache(TEST_CAPACITY);

        // when
        lruCache.put("key 5", new Item("new value"));

        // then
        assertNotEquals(0, lruCache.getKeys().indexOf("key 5"));
    }

    @Test
    public void shouldPutNewKeyToTheZeroIndex() {

        // given
        Item zeroItem = new Item("value 0");
        lruCache.put("key 0", zeroItem);
        Item firstItem = new Item("value 1");
        lruCache.put("key 1", firstItem);
        Item secondItem = new Item("value 2");
        lruCache.put("key 2", secondItem);

        // when
        lruCache.get("key 1");
        lruCache.get("key 2");
        Item thirdItem = new Item("value 3");
        lruCache.put("key 3", thirdItem);

        // then
        assertEquals("key 3", lruCache.getKeys().get(0));
        assertEquals("key 2", lruCache.getKeys().get(1));
        assertEquals("key 1", lruCache.getKeys().get(2));
        assertEquals("key 0", lruCache.getKeys().get(3));
        assertEquals(4, lruCache.getKeys().size());
    }

    @Test
    public void shouldReturnCorrectSize() {
        // given
        insertDataToCache(TEST_CAPACITY);

        // then
        assertEquals(TEST_CAPACITY, lruCache.size());
    }


    private void insertDataToCache(int capacity)
    {
        for (int i = 0; i < capacity; i++) {
            lruCache.put("key " + i, new Item("value " + i));
        }
    }
}