package net.vmuzychko;

import java.util.Optional;


public interface Cache<K, T>
{
    void put(K key, T value);

    Optional<T> get(K key);

    void evict(K key);

    void clear();

    int size();

    void printKeysState();
}
