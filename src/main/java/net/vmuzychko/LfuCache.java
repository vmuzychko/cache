package net.vmuzychko;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Least-Frequently Used (LFU)
 * When the cache is full and requires more room the system will purge the item with the lowest reference frequency.
 */
public class LfuCache<K, T> implements Cache<K, T>
{
    private final int capacity;
    private Map<K, Integer> keys;
    private Map<K, T> data;

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();

    public LfuCache(int capacity)
    {
        this.capacity = capacity;
        keys = new HashMap<>();
        data = new HashMap<>();
    }

    @Override
    public void put(K key, T value)
    {
        writeLock.lock();
        try
        {
            if (keys.size() == capacity)
                invalidateLeastFrequentlyUsedElement();
            if (keys.get(key) == null)
                keys.put(key, 0);
            data.put(key, value);
        }
        finally
        {
            writeLock.unlock();
        }
    }

    @Override
    public Optional<T> get(K key)
    {
        writeLock.lock();
        try
        {
            if (keys.containsKey(key)) {
                Integer oldFrequency = keys.get(key);
                keys.put(key, oldFrequency.intValue() + 1);
                return Optional.ofNullable(data.get(key));
            } else {
                return Optional.empty();
            }
        }
        finally
        {
            writeLock.unlock();
        }
    }

    @Override
    public void evict(K key)
    {
        writeLock.lock();
        try
        {
            if (keys.containsKey(key))
            {
                keys.remove(key);
                data.remove(key);
            }
            else
            {
                throw new NoSuchElementException("Cache doesn't contain data with key: " + key);
            }
        }
        finally
        {
            writeLock.unlock();
        }
    }

    @Override
    public void clear()
    {
        writeLock.lock();
        try
        {
            keys.clear();
            data.clear();
        }
        finally
        {
            writeLock.unlock();
        }
    }

    @Override
    public int size()
    {
        readLock.lock();
        try
        {
            return data.size();
        }
        finally
        {
            readLock.unlock();
        }
    }

    @Override
    public void printKeysState()
    {
        readLock.lock();
        try
        {
            keys.forEach((key, frequency) -> System.out.println("Key: " + key + " Frequency: " + frequency));
        }
        finally
        {
            readLock.unlock();
        }
    }

    public Map<K, Integer> getKeys()
    {
        readLock.lock();
        try
        {
            return Collections.unmodifiableMap(keys);
        }
        finally
        {
            readLock.unlock();
        }
    }

    private void invalidateLeastFrequentlyUsedElement()
    {
        Comparator<Map.Entry<K, Integer>> keysByFrequencyComparator = (x, y) -> x.getValue().compareTo(y.getValue());
        K leastFrequentlyUsedKey = keys.entrySet().stream().min(keysByFrequencyComparator).get().getKey();
        keys.remove(leastFrequentlyUsedKey);
        data.remove(leastFrequentlyUsedKey);
    }
}
