package net.vmuzychko;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Least Recently Used (LRU)
 * Invalidates the least recently used items first
 */
public class LruCache<K, T> implements Cache<K, T>
{
    private final int capacity;
    private List<K> keys;
    private Map<K, T> data;

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();

    public LruCache(int capacity)
    {
        this.capacity = capacity;
        keys = new ArrayList<>();
        data = new HashMap<>();
    }

    /**
     * Puts value to the data and key to the 0 position of key list.
     * In case key is already present it's position won't change
     * Invalidates the least recently used item before inserting a new item
     * if cache size reached its capacity.
     *
     * @param key
     * @param value
     */
    @Override
    public void put(K key, T value)
    {
        writeLock.lock();
        try
        {
            boolean keyIsAbsent = keys.indexOf(key) < 0;
            if (keys.size() == capacity && keyIsAbsent)
                invalidate();
            if (keyIsAbsent)
                keys.add(0, key);

            data.put(key, value);
        }
        finally
        {
            writeLock.unlock();
        }
    }

    /**
     * Returns value by key specified and moves key to the 0 position of key list
     *
     * @param key
     * @return
     */
    @Override
    public Optional<T> get(K key)
    {
        writeLock.lock();
        try
        {
            int oldKeyIndex = keys.indexOf(key);
            if (oldKeyIndex >= 0)
            {
                K lastRequestedKey = keys.get(oldKeyIndex);
                keys.remove(oldKeyIndex);
                keys.add(0, lastRequestedKey);
                return Optional.ofNullable(data.get(key));
            }
            else
            {
                return Optional.empty();
            }
        }
        finally
        {
            writeLock.unlock();
        }
    }

    /**
     * Removes value by key specified if key exists
     *
     * @param key
     */
    @Override
    public void evict(K key)
    {
        writeLock.lock();
        try
        {
            if (keys.contains(key))
            {
                keys.remove(keys.indexOf(key));
                data.remove(key);
            }
            else
            {
                throw new NoSuchElementException("Cache doesn't contain data with key: " + key);
            }
        }
        finally
        {
            writeLock.unlock();
        }
    }

    /**
     * Clears the cache
     */
    @Override
    public void clear()
    {
        writeLock.lock();
        try
        {
            keys.clear();
            data.clear();
        }
        finally
        {
            writeLock.unlock();
        }
    }

    @Override
    public int size()
    {
        readLock.lock();
        try
        {
            return keys.size();
        }
        finally
        {
            readLock.unlock();
        }
    }

    @Override
    public void printKeysState()
    {
        readLock.lock();
        try
        {
            keys.forEach(key -> System.out.println("key: " + key));
        }
        finally
        {
            readLock.unlock();
        }
    }

    public List<K> getKeys()
    {
        readLock.lock();
        try
        {
            return Collections.unmodifiableList(keys);
        }
        finally
        {
            readLock.unlock();
        }
    }

    /**
     * Invalidates the least recently used item.
     */
    private void invalidate()
    {
        K leastRecentlyUsedKey = keys.get(keys.size() - 1);
        data.remove(leastRecentlyUsedKey);
        keys.remove(leastRecentlyUsedKey);
    }
}
